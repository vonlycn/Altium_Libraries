Var
    CurrentSCHLib : ISch_Lib;
    CurrentLib : IPCB_Library;

Procedure CreateTHComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, HoleType : TExtendedHoleType,
                               HoleSize : Real, HoleLength : Real, Layer : TLayer, X : Real, Y : Real,
                               OffsetX : Real, OffsetY : Real, TopShape : TShape, TopXSize : Real, TopYSize : Real,
                               InnerShape : TShape, InnerXSize : Real, InnerYSize : Real,
                               BottomShape : TShape, BottomXSize : Real, BottomYSize : Real,
                               Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion: Real, Plated : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleType := HoleType;
    NewPad.HoleSize := MilsToCoord(HoleSize);
    if HoleLength <> 0 then
        NewPad.HoleWidth := MilsToCoord(HoleLength);
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    if BottomShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eBottomLayer, CRRatio);
    NewPad.TopXSize := MilsToCoord(TopXSize);
    NewPad.TopYSize := MilsToCoord(TopYSize);
    NewPad.MidShape := InnerShape;
    NewPad.MidXSize := MilsToCoord(InnerXSize);
    NewPad.MidYSize := MilsToCoord(InnerYSize);
    NewPad.BotShape := BottomShape;
    NewPad.BotXSize := MilsToCoord(BottomXSize);
    NewPad.BotYSize := MilsToCoord(BottomYSize);
    NewPad.SetState_XPadOffsetOnLayer(Layer, MilsToCoord(OffsetX));
    NewPad.SetState_YPadOffsetOnLayer(Layer, MilsToCoord(OffsetY));
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MilsToCoord(X), MilsToCoord(Y));
    NewPad.Plated   := Plated;
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if PMExpansion <> 0 then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MilsToCoord(PMExpansion);
    End;
    if SMExpansion <> 0 then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MilsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateComponentTrack(NewPCBLibComp : IPCB_LibComponent, X1 : Real, Y1 : Real, X2 : Real, Y2 : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewTrack                    : IPCB_Track;

Begin
    NewTrack := PcbServer.PCBObjectFactory(eTrackObject,eNoDimension,eCreate_Default);
    NewTrack.X1 := MilsToCoord(X1);
    NewTrack.Y1 := MilsToCoord(Y1);
    NewTrack.X2 := MilsToCoord(X2);
    NewTrack.Y2 := MilsToCoord(Y2);
    NewTrack.Layer := Layer;
    NewTrack.Width := MilsToCoord(LineWidth);
    NewTrack.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewTrack);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewTrack.I_ObjectAddress);
End;

Procedure CreateComponentArc(NewPCBLibComp : IPCB_LibComponent, CenterX : Real, CenterY : Real, Radius : Real, StartAngle : Real, EndAngle : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewArc                      : IPCB_Arc;

Begin
    NewArc := PCBServer.PCBObjectFactory(eArcObject,eNoDimension,eCreate_Default);
    NewArc.XCenter := MilsToCoord(CenterX);
    NewArc.YCenter := MilsToCoord(CenterY);
    NewArc.Radius := MilsToCoord(Radius);
    NewArc.StartAngle := StartAngle;
    NewArc.EndAngle := EndAngle;
    NewArc.Layer := Layer;
    NewArc.LineWidth := MilsToCoord(LineWidth);
    NewArc.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewArc);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewArc.I_ObjectAddress);
End;

Function ReadStringFromIniFile(Section: String, Name: String, FilePath: String, IfEmpty: String) : String;
Var
    IniFile                     : TIniFile;

Begin
    result := IfEmpty;
    If FileExists(FilePath) Then
    Begin
        Try
            IniFile := TIniFile.Create(FilePath);

            Result := IniFile.ReadString(Section, Name, IfEmpty);
        Finally
            Inifile.Free;
        End;
    End;
End;

Procedure EnableMechanicalLayers(Zero : Integer);
Var
    Board                       : IPCB_Board;
    MajorADVersion              : Integer;

Begin
End;

Procedure CreateComponentHDRV7(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'HDRV7';
        NewPCBLibComp.Description := 'Header, Vertical, 100 mil pitch; 25.2 mil lead width, 7 pins, 1 row, 7 pins per row, 700 mil L X 100 mil W X 329.9 mil H body';
        NewPCBLibComp.Height := MilsToCoord(329.8993402);

        CreateTHComponentPad(NewPCBLibComp, '1', eRoundHole, 41.5999168, 0, eBottomLayer, 0, 0, 0, 0, eRectangular, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRectangular, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '2', eRoundHole, 41.5999168, 0, eBottomLayer, 99.9998, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '3', eRoundHole, 41.5999168, 0, eBottomLayer, 199.9996, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '4', eRoundHole, 41.5999168, 0, eBottomLayer, 299.9994, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '5', eRoundHole, 41.5999168, 0, eBottomLayer, 399.9992, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '6', eRoundHole, 41.5999168, 0, eBottomLayer, 499.999, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);
        CreateTHComponentPad(NewPCBLibComp, '7', eRoundHole, 41.5999168, 0, eBottomLayer, 599.9988, 0, 0, 0, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, eRounded, 62.3998752, 62.3998752, 0, 0, -62.3999, 0, True);

        CreateComponentTrack(NewPCBLibComp, -12.6, -12.6, -12.6, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, -12.6, 12.6, 12.6, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 12.6, 12.6, 12.6, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 12.6, -12.6, -12.6, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 87.3998, -12.6, 87.3998, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 87.3998, 12.6, 112.5998, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 112.5998, 12.6, 112.5998, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 112.5998, -12.6, 87.3998, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 187.3996, -12.6, 187.3996, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 187.3996, 12.6, 212.5996, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 212.5996, 12.6, 212.5996, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 212.5996, -12.6, 187.3996, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 287.3994, -12.6, 287.3994, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 287.3994, 12.6, 312.5994, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 312.5994, 12.6, 312.5994, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 312.5994, -12.6, 287.3994, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 387.3992, -12.6, 387.3992, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 387.3992, 12.6, 412.5992, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 412.5992, 12.6, 412.5992, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 412.5992, -12.6, 387.3992, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 487.399, -12.6, 487.399, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 487.399, 12.6, 512.599, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 512.599, 12.6, 512.599, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 512.599, -12.6, 487.399, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 587.3988, -12.6, 587.3988, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 587.3988, 12.6, 612.5988, 12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 612.5988, 12.6, 612.5988, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 612.5988, -12.6, 587.3988, -12.6, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, -49.9999, -49.9999, 49.9999, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, 49.9999, 649.9987, 49.9999, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, 49.9999, 649.9987, -49.9999, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, -49.9999, -49.9999, -49.9999, eMechanical12, 1, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, -49.9999, -49.9999, 49.9999, eTopOverlay, 4.7, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, 49.9999, 649.9987, 49.9999, eTopOverlay, 4.7, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, 49.9999, 649.9987, -49.9999, eTopOverlay, 4.7, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, -49.9999, -49.9999, -49.9999, eTopOverlay, 4.7, False);
        CreateComponentTrack(NewPCBLibComp, -59.9999, -59.9999, -59.9999, 59.9999, eMechanical15, 2, False);
        CreateComponentTrack(NewPCBLibComp, -59.9999, 59.9999, 659.9987, 59.9999, eMechanical15, 2, False);
        CreateComponentTrack(NewPCBLibComp, 659.9987, 59.9999, 659.9987, -59.9999, eMechanical15, 2, False);
        CreateComponentTrack(NewPCBLibComp, 659.9987, -59.9999, -59.9999, -59.9999, eMechanical15, 2, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 12, 0, 360, eMechanical15, 2, False);
        CreateComponentTrack(NewPCBLibComp, 15.999968, 0, -15.999968, 0, eMechanical15, 1.999996, False);
        CreateComponentTrack(NewPCBLibComp, 0, 15.999968, 0, -15.999968, eMechanical15, 1.999996, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, -49.9999, -49.9999, 49.9999, eMechanical11, 4.6999906, False);
        CreateComponentTrack(NewPCBLibComp, -49.9999, 49.9999, 649.9987, 49.9999, eMechanical11, 4.6999906, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, 49.9999, 649.9987, -49.9999, eMechanical11, 4.6999906, False);
        CreateComponentTrack(NewPCBLibComp, 649.9987, -49.9999, -49.9999, -49.9999, eMechanical11, 4.6999906, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\HDRV7.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;

Procedure CreateAPCBLibrary(Zero : integer);
Var
    View     : IServerDocumentView;
    Document : IServerDocument;

Begin
    If PCBServer = Nil Then
    Begin
        ShowMessage('No PCBServer present. This script inserts a footprint into an existing PCB Library that has the current focus.');
        Exit;
    End;

    CurrentLib := PcbServer.GetCurrentPCBLibrary;
    If CurrentLib = Nil Then
    Begin
        ShowMessage('You must have focus on a PCB Library in order for this script to run.');
        Exit;
    End;

    View := Client.GetCurrentView;
    Document := View.OwnerDocument;
    Document.Modified := True;

    CreateComponentHDRV7(0);
End;

Procedure CreateALibrary;
Begin
    Screen.Cursor := crHourGlass;

    CreateAPCBLibrary(0);

    Screen.Cursor := crArrow;
End;

End.
