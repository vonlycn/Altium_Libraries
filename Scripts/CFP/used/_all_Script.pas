Var
    CurrentSCHLib : ISch_Lib;
    CurrentLib : IPCB_Library;

Function CreateAComponent(Name: String) : IPCB_LibComponent;
Var
    PrimitiveList: TInterfaceList;
    PrimitiveIterator: IPCB_GroupIterator;
    PrimitiveHandle: IPCB_Primitive;
    I:  Integer;

Begin
    // Check if footprint already in library
    Result := CurrentLib.GetComponentByName(Name);
    If Result = Nil Then
    Begin
        // Create New Component
        Result := PCBServer.CreatePCBLibComp;
        Result.Name := Name;
    End
    Else
    Begin
        // Clear existin component
        Try
            // Create List with all primitives on board
            PrimitiveList := TInterfaceList.Create;
            PrimitiveIterator := Result.GroupIterator_Create;
            PrimitiveIterator.AddFilter_ObjectSet(AllObjects);
            PrimitiveHandle := PrimitiveIterator.FirstPCBObject;
            While PrimitiveHandle <> Nil Do
            Begin
                PrimitiveList.Add(PrimitiveHandle);
                PrimitiveHandle := PrimitiveIterator.NextPCBObject;
            End;

            // Delete all primitives
            For I := 0 To PrimitiveList.Count - 1 Do
            Begin
                PrimitiveHandle := PrimitiveList.items[i];
                Result.RemovePCBObject(PrimitiveHandle);
                Result.GraphicallyInvalidate;
            End;

        Finally
            Result.GroupIterator_Destroy(PrimitiveIterator);
            PrimitiveList.Free;
        End;
    End;
End; 


Procedure CreateTHComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, HoleType : TExtendedHoleType,
                               HoleSize : Real, HoleLength : Real, Layer : TLayer, X : Real, Y : Real,
                               OffsetX : Real, OffsetY : Real, TopShape : TShape, TopXSize : Real, TopYSize : Real,
                               InnerShape : TShape, InnerXSize : Real, InnerYSize : Real,
                               BottomShape : TShape, BottomXSize : Real, BottomYSize : Real,
                               Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion: Real, Plated : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleType := HoleType;
    NewPad.HoleSize := MMsToCoord(HoleSize);
    if HoleLength <> 0 then
        NewPad.HoleWidth := MMsToCoord(HoleLength);
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    if BottomShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eBottomLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.MidShape := InnerShape;
    NewPad.MidXSize := MMsToCoord(InnerXSize);
    NewPad.MidYSize := MMsToCoord(InnerYSize);
    NewPad.BotShape := BottomShape;
    NewPad.BotXSize := MMsToCoord(BottomXSize);
    NewPad.BotYSize := MMsToCoord(BottomYSize);
    NewPad.SetState_XPadOffsetOnLayer(Layer, MMsToCoord(OffsetX));
    NewPad.SetState_YPadOffsetOnLayer(Layer, MMsToCoord(OffsetY));
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Plated   := Plated;
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if PMExpansion <> 0 then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if SMExpansion <> 0 then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateSMDComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, Layer : TLayer, X : Real, Y : Real, OffsetX : Real, OffsetY : Real,
                                TopShape : TShape, TopXSize : Real, TopYSize : Real, Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion : Real,
                                PMFromRules : Boolean, SMFromRules : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleSize := MMsToCoord(0);
    NewPad.Layer    := Layer;
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if (PMExpansion <> 0) or (PMFromRules = False) then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if (SMExpansion <> 0) or (SMFromRules = False) then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateComponentTrack(NewPCBLibComp : IPCB_LibComponent, X1 : Real, Y1 : Real, X2 : Real, Y2 : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewTrack                    : IPCB_Track;

Begin
    NewTrack := PcbServer.PCBObjectFactory(eTrackObject,eNoDimension,eCreate_Default);
    NewTrack.X1 := MMsToCoord(X1);
    NewTrack.Y1 := MMsToCoord(Y1);
    NewTrack.X2 := MMsToCoord(X2);
    NewTrack.Y2 := MMsToCoord(Y2);
    NewTrack.Layer := Layer;
    NewTrack.Width := MMsToCoord(LineWidth);
    NewTrack.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewTrack);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewTrack.I_ObjectAddress);
End;

Procedure CreateComponentArc(NewPCBLibComp : IPCB_LibComponent, CenterX : Real, CenterY : Real, Radius : Real, StartAngle : Real, EndAngle : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewArc                      : IPCB_Arc;

Begin
    NewArc := PCBServer.PCBObjectFactory(eArcObject,eNoDimension,eCreate_Default);
    NewArc.XCenter := MMsToCoord(CenterX);
    NewArc.YCenter := MMsToCoord(CenterY);
    NewArc.Radius := MMsToCoord(Radius);
    NewArc.StartAngle := StartAngle;
    NewArc.EndAngle := EndAngle;
    NewArc.Layer := Layer;
    NewArc.LineWidth := MMsToCoord(LineWidth);
    NewArc.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewArc);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewArc.I_ObjectAddress);
End;

Function ReadStringFromIniFile(Section: String, Name: String, FilePath: String, IfEmpty: String) : String;
Var
    IniFile                     : TIniFile;

Begin
    result := IfEmpty;
    If FileExists(FilePath) Then
    Begin
        Try
            IniFile := TIniFile.Create(FilePath);

            Result := IniFile.ReadString(Section, Name, IfEmpty);
        Finally
            Inifile.Free;
        End;
    End;
End;

Procedure EnableMechanicalLayers(Zero : Integer);
Var
    Board                       : IPCB_Board;
    MajorADVersion              : Integer;

Begin
End;


Procedure CreateComponentSW2_54_1x2(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW254_1x2');
        NewPcbLibComp.Name := 'SW254_1x2';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP); 2 pin, 3.80 mm L X 6.45 mm W X 4.55 mm H body';
        NewPCBLibComp.Height := MMsToCoord(4.55);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, 0, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.8, -3, 0.8, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -3, 0.8, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -5, -0.8, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -5, -0.8, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 3, -0.8, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 3, -0.8, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 5, 0.8, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 5, 0.8, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -3.225, -1.9, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, 3.225, 1.9, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 3.225, 1.9, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -3.225, -1.9, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -3.23, -1.9, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, 3.23, 1.9, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 3.23, 1.9, -3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -3.23, -1.9, -3.23, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.095, -3.23, 1.9, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -3.23, 1.9, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 3.23, 1.095, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.095, -3.23, -1.9, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -3.23, -1.9, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, 3.23, -1.095, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.1, -3.43, 2.1, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.1, 3.43, 1.115, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.115, 3.43, 1.115, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.115, 5.655, -1.115, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.115, 5.655, -1.115, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.115, 3.43, -2.1, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.1, 3.43, -2.1, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.1, -3.43, -1.115, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.115, -3.43, -1.115, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.115, -5.655, 1.115, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.115, -5.655, 1.115, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.115, -3.43, 2.1, -3.43, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\CFP\SW254_1x2.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW2_54_2x2(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW254_2x2');
        NewPcbLibComp.Name := 'SW254_2x2';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 2.54 mm pitch; 4 pin, 6.20 mm L X 6.45 mm W X 4.55 mm H body';
        NewPCBLibComp.Height := MMsToCoord(4.55);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.27, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 1.27, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 1.27, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, -1.27, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.07, -3, -0.47, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, -3, -0.47, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, -5, -2.07, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, -5, -2.07, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, -3, 2.07, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, -3, 2.07, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, -5, 0.47, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, -5, 0.47, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, 3, 0.47, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, 3, 0.47, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, 5, 2.07, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, 5, 2.07, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, 3, -2.07, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, 3, -2.07, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, 5, -0.47, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, 5, -0.47, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, -3.225, -3.1, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, 3.225, 3.1, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, 3.225, 3.1, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, -3.225, -3.1, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, -3.23, -3.1, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, 3.23, 3.1, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, 3.23, 3.1, -3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, -3.23, -3.1, -3.23, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.365, -3.23, 3.1, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, -3.23, 3.1, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.1, 3.23, 2.365, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.365, -3.23, -3.1, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, -3.23, -3.1, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.1, 3.23, -2.365, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, -3.43, 3.3, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, 3.43, 2.385, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.385, 3.43, 2.385, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.385, 5.655, -2.385, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.385, 5.655, -2.385, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.385, 3.43, -3.3, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, 3.43, -3.3, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, -3.43, -2.385, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.385, -3.43, -2.385, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.385, -5.655, 2.385, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.385, -5.655, 2.385, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.385, -3.43, 3.3, -3.43, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\CFP\SW254_2x2.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW2_54_3x2(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW254_3x2');
        NewPcbLibComp.Name := 'SW254_3x2';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 2.54 mm pitch; 6 pin, 8.70 mm L X 6.45 mm W X 4.55 mm H body';
        NewPCBLibComp.Height := MMsToCoord(4.55);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -2.54, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 2.54, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 2.54, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 0, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, -2.54, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -3.34, -3, -1.74, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.74, -3, -1.74, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.74, -5, -3.34, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.34, -5, -3.34, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -3, 0.8, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -3, 0.8, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -5, -0.8, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -5, -0.8, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.74, -3, 3.34, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.34, -3, 3.34, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.34, -5, 1.74, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.74, -5, 1.74, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.34, 3, 1.74, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.74, 3, 1.74, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.74, 5, 3.34, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.34, 5, 3.34, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 3, -0.8, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 3, -0.8, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 5, 0.8, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 5, 0.8, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.74, 3, -3.34, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.34, 3, -3.34, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.34, 5, -1.74, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.74, 5, -1.74, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, -3.225, -4.35, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, 3.225, 4.35, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, 3.225, 4.35, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, -3.225, -4.35, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, -3.23, -4.35, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, 3.23, 4.35, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, 3.23, 4.35, -3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, -3.23, -4.35, -3.23, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.635, -3.23, 4.35, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, -3.23, 4.35, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.35, 3.23, 3.635, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.635, -3.23, -4.35, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, -3.23, -4.35, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.35, 3.23, -3.635, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.55, -3.43, 4.55, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.55, 3.43, 3.655, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.655, 3.43, 3.655, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.655, 5.655, -3.655, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.655, 5.655, -3.655, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.655, 3.43, -4.55, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.55, 3.43, -4.55, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.55, -3.43, -3.655, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.655, -3.43, -3.655, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.655, -5.655, 3.655, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.655, -5.655, 3.655, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.655, -3.43, 4.55, -3.43, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\CFP\SW254_3x2.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW2_54_4x2(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW254_4x2');
        NewPcbLibComp.Name := 'SW254_4x2';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 2.54 mm pitch; 8 pin, 11.50 mm L X 6.45 mm W X 4.55 mm H body';
        NewPCBLibComp.Height := MMsToCoord(4.55);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -3.81, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -1.27, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 1.27, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 3.81, -4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 270, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 3.81, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 1.27, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, -1.27, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, -3.81, 4.125, 0, 0, eRoundedRectangular, 2.66, 1.83, 90, 27.32, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -4.61, -3, -3.01, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.01, -3, -3.01, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.01, -5, -4.61, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.61, -5, -4.61, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, -3, -0.47, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, -3, -0.47, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, -5, -2.07, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, -5, -2.07, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, -3, 2.07, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, -3, 2.07, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, -5, 0.47, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, -5, 0.47, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.01, -3, 4.61, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.61, -3, 4.61, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.61, -5, 3.01, -5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.01, -5, 3.01, -3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.61, 3, 3.01, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.01, 3, 3.01, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.01, 5, 4.61, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.61, 5, 4.61, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, 3, 0.47, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, 3, 0.47, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.47, 5, 2.07, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.07, 5, 2.07, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, 3, -2.07, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, 3, -2.07, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.07, 5, -0.47, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.47, 5, -0.47, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.01, 3, -4.61, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.61, 3, -4.61, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.61, 5, -3.01, 5, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.01, 5, -3.01, 3, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, -3.225, -5.75, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, 3.225, 5.75, 3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, 3.225, 5.75, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, -3.225, -5.75, -3.225, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, -3.23, -5.75, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, 3.23, 5.75, 3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, 3.23, 5.75, -3.23, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, -3.23, -5.75, -3.23, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.905, -3.23, 5.75, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, -3.23, 5.75, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.75, 3.23, 4.905, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.905, -3.23, -5.75, -3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, -3.23, -5.75, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.75, 3.23, -4.905, 3.23, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.95, -3.43, 5.95, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.95, 3.43, 4.925, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.925, 3.43, 4.925, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.925, 5.655, -4.925, 5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.925, 5.655, -4.925, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.925, 3.43, -5.95, 3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.95, 3.43, -5.95, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.95, -3.43, -4.925, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.925, -3.43, -4.925, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.925, -5.655, 4.925, -5.655, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.925, -5.655, 4.925, -3.43, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.925, -3.43, 5.95, -3.43, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\CFP\SW254_4x2.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;

Procedure CreateAPCBLibrary(Zero : integer);
Var
    View     : IServerDocumentView;
    Document : IServerDocument;

Begin
    If PCBServer = Nil Then
    Begin
        ShowMessage('No PCBServer present. This script inserts a footprint into an existing PCB Library that has the current focus.');
        Exit;
    End;

    CurrentLib := PcbServer.GetCurrentPCBLibrary;
    If CurrentLib = Nil Then
    Begin
        ShowMessage('You must have focus on a PCB Library in order for this script to run.');
        Exit;
    End;

    View := Client.GetCurrentView;
    Document := View.OwnerDocument;
    Document.Modified := True;


		CreateComponentSW2_54_1x2(0);
		CreateComponentSW2_54_2x2(0);
		CreateComponentSW2_54_3x2(0);
		CreateComponentSW2_54_4x2(0);
    
End;

Procedure CreateALibrary;
Begin
    Screen.Cursor := crHourGlass;

    CreateAPCBLibrary(0);

    Screen.Cursor := crArrow;
End;

End.