Var
    CurrentSCHLib : ISch_Lib;
    CurrentLib : IPCB_Library;

Function CreateAComponent(Name: String) : IPCB_LibComponent;
Var
    PrimitiveList: TInterfaceList;
    PrimitiveIterator: IPCB_GroupIterator;
    PrimitiveHandle: IPCB_Primitive;
    I:  Integer;

Begin
    // Check if footprint already in library
    Result := CurrentLib.GetComponentByName(Name);
    If Result = Nil Then
    Begin
        // Create New Component
        Result := PCBServer.CreatePCBLibComp;
        Result.Name := Name;
    End
    Else
    Begin
        // Clear existin component
        Try
            // Create List with all primitives on board
            PrimitiveList := TInterfaceList.Create;
            PrimitiveIterator := Result.GroupIterator_Create;
            PrimitiveIterator.AddFilter_ObjectSet(AllObjects);
            PrimitiveHandle := PrimitiveIterator.FirstPCBObject;
            While PrimitiveHandle <> Nil Do
            Begin
                PrimitiveList.Add(PrimitiveHandle);
                PrimitiveHandle := PrimitiveIterator.NextPCBObject;
            End;

            // Delete all primitives
            For I := 0 To PrimitiveList.Count - 1 Do
            Begin
                PrimitiveHandle := PrimitiveList.items[i];
                Result.RemovePCBObject(PrimitiveHandle);
                Result.GraphicallyInvalidate;
            End;

        Finally
            Result.GroupIterator_Destroy(PrimitiveIterator);
            PrimitiveList.Free;
        End;
    End;
End; 


Procedure CreateTHComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, HoleType : TExtendedHoleType,
                               HoleSize : Real, HoleLength : Real, Layer : TLayer, X : Real, Y : Real,
                               OffsetX : Real, OffsetY : Real, TopShape : TShape, TopXSize : Real, TopYSize : Real,
                               InnerShape : TShape, InnerXSize : Real, InnerYSize : Real,
                               BottomShape : TShape, BottomXSize : Real, BottomYSize : Real,
                               Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion: Real, Plated : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleType := HoleType;
    NewPad.HoleSize := MMsToCoord(HoleSize);
    if HoleLength <> 0 then
        NewPad.HoleWidth := MMsToCoord(HoleLength);
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    if BottomShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eBottomLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.MidShape := InnerShape;
    NewPad.MidXSize := MMsToCoord(InnerXSize);
    NewPad.MidYSize := MMsToCoord(InnerYSize);
    NewPad.BotShape := BottomShape;
    NewPad.BotXSize := MMsToCoord(BottomXSize);
    NewPad.BotYSize := MMsToCoord(BottomYSize);
    NewPad.SetState_XPadOffsetOnLayer(Layer, MMsToCoord(OffsetX));
    NewPad.SetState_YPadOffsetOnLayer(Layer, MMsToCoord(OffsetY));
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Plated   := Plated;
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if PMExpansion <> 0 then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if SMExpansion <> 0 then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateSMDComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, Layer : TLayer, X : Real, Y : Real, OffsetX : Real, OffsetY : Real,
                                TopShape : TShape, TopXSize : Real, TopYSize : Real, Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion : Real,
                                PMFromRules : Boolean, SMFromRules : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleSize := MMsToCoord(0);
    NewPad.Layer    := Layer;
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if (PMExpansion <> 0) or (PMFromRules = False) then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if (SMExpansion <> 0) or (SMFromRules = False) then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateComponentTrack(NewPCBLibComp : IPCB_LibComponent, X1 : Real, Y1 : Real, X2 : Real, Y2 : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewTrack                    : IPCB_Track;

Begin
    NewTrack := PcbServer.PCBObjectFactory(eTrackObject,eNoDimension,eCreate_Default);
    NewTrack.X1 := MMsToCoord(X1);
    NewTrack.Y1 := MMsToCoord(Y1);
    NewTrack.X2 := MMsToCoord(X2);
    NewTrack.Y2 := MMsToCoord(Y2);
    NewTrack.Layer := Layer;
    NewTrack.Width := MMsToCoord(LineWidth);
    NewTrack.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewTrack);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewTrack.I_ObjectAddress);
End;

Procedure CreateComponentArc(NewPCBLibComp : IPCB_LibComponent, CenterX : Real, CenterY : Real, Radius : Real, StartAngle : Real, EndAngle : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewArc                      : IPCB_Arc;

Begin
    NewArc := PCBServer.PCBObjectFactory(eArcObject,eNoDimension,eCreate_Default);
    NewArc.XCenter := MMsToCoord(CenterX);
    NewArc.YCenter := MMsToCoord(CenterY);
    NewArc.Radius := MMsToCoord(Radius);
    NewArc.StartAngle := StartAngle;
    NewArc.EndAngle := EndAngle;
    NewArc.Layer := Layer;
    NewArc.LineWidth := MMsToCoord(LineWidth);
    NewArc.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewArc);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewArc.I_ObjectAddress);
End;

Function ReadStringFromIniFile(Section: String, Name: String, FilePath: String, IfEmpty: String) : String;
Var
    IniFile                     : TIniFile;

Begin
    result := IfEmpty;
    If FileExists(FilePath) Then
    Begin
        Try
            IniFile := TIniFile.Create(FilePath);

            Result := IniFile.ReadString(Section, Name, IfEmpty);
        Finally
            Inifile.Free;
        End;
    End;
End;

Procedure EnableMechanicalLayers(Zero : Integer);
Var
    Board                       : IPCB_Board;
    MajorADVersion              : Integer;

Begin
End;


Procedure CreateComponentSW1_27_10P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-10P');
        NewPcbLibComp.Name := 'SW1.27-10P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 20 pin, 13.84 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -5.715, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -4.445, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, -3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, -1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, -0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, 1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, 3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '9', eTopLayer, 4.445, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '10', eTopLayer, 5.715, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '11', eTopLayer, 5.715, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '12', eTopLayer, 4.445, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '13', eTopLayer, 3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '14', eTopLayer, 1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '15', eTopLayer, 0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '16', eTopLayer, -0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '17', eTopLayer, -1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '18', eTopLayer, -3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '19', eTopLayer, -4.445, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '20', eTopLayer, -5.715, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -5.965, -3.85, -5.465, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.465, -3.85, -5.465, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.465, -4.45, -5.965, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.965, -4.45, -5.965, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, -3.85, -4.195, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, -3.85, -4.195, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, -4.45, -4.695, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, -4.45, -4.695, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, -3.85, -2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -3.85, -2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -4.45, -3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, -4.45, -3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -3.85, -1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -3.85, -1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -4.45, -2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -4.45, -2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -3.85, -0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -3.85, -0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -4.45, -0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -4.45, -0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -3.85, 0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -3.85, 0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -4.45, 0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -4.45, 0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -3.85, 2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -3.85, 2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -4.45, 1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -4.45, 1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -3.85, 3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -3.85, 3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -4.45, 2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -4.45, 2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, -3.85, 4.695, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, -3.85, 4.695, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, -4.45, 4.195, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, -4.45, 4.195, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.465, -3.85, 5.965, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.965, -3.85, 5.965, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.965, -4.45, 5.465, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.465, -4.45, 5.465, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.965, 3.85, 5.465, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.465, 3.85, 5.465, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.465, 4.45, 5.965, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.965, 4.45, 5.965, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, 3.85, 4.195, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, 3.85, 4.195, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, 4.45, 4.695, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, 4.45, 4.695, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 3.85, 2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 3.85, 2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 4.45, 3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 4.45, 3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 3.85, 1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 3.85, 1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 4.45, 2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 4.45, 2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 3.85, 0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 3.85, 0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 4.45, 0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 4.45, 0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 3.85, -0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 3.85, -0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 4.45, -0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 4.45, -0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 3.85, -2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 3.85, -2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 4.45, -1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 4.45, -1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 3.85, -3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 3.85, -3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 4.45, -2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 4.45, -2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, 3.85, -4.695, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, 3.85, -4.695, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, 4.45, -4.195, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, 4.45, -4.195, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.465, 3.85, -5.965, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.965, 3.85, -5.965, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.965, 4.45, -5.465, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.465, 4.45, -5.465, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -6.92, -2.9, -6.92, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -6.92, 2.9, 6.92, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 6.92, 2.9, 6.92, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 6.92, -2.9, -6.92, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7, -3.15, -7, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -7, 3.15, 7, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7, 3.15, 7, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7, -3.15, -7, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.27, -3.15, 7, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7, -3.15, 7, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7, 3.15, 6.27, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -6.27, -3.15, -7, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -7, -3.15, -7, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -7, 3.15, -6.27, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7.2, -3.35, 7.2, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 7.2, 3.35, 6.29, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.29, 3.35, 6.29, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.29, 5.155, -6.29, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.29, 5.155, -6.29, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.29, 3.35, -7.2, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -7.2, 3.35, -7.2, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -7.2, -3.35, -6.29, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.29, -3.35, -6.29, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.29, -5.155, 6.29, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.29, -5.155, 6.29, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.29, -3.35, 7.2, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-10P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_1P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-1P');
        NewPcbLibComp.Name := 'SW1.27-1P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP); 2 pin, 2.41 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, 0, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.25, -3.85, 0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -3.85, 0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -4.45, -0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, -4.45, -0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 3.85, -0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 3.85, -0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 4.45, 0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 4.45, 0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.205, -2.9, -1.205, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.205, 2.9, 1.205, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.205, 2.9, 1.205, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.205, -2.9, -1.205, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.28, -3.15, -1.28, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.28, 3.15, 1.28, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.28, 3.15, 1.28, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.28, -3.15, -1.28, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.555, -3.15, 1.28, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.28, -3.15, 1.28, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.28, 3.15, 0.555, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.555, -3.15, -1.28, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.28, -3.15, -1.28, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.28, 3.15, -0.555, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.48, -3.35, 1.48, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.48, 3.35, 0.575, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.575, 3.35, 0.575, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.575, 5.155, -0.575, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.575, 5.155, -0.575, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.575, 3.35, -1.48, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.48, 3.35, -1.48, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.48, -3.35, -0.575, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.575, -3.35, -0.575, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.575, -5.155, 0.575, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.575, -5.155, 0.575, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.575, -3.35, 1.48, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-1P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_2P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-2P');
        NewPcbLibComp.Name := 'SW1.27-2P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 4 pin, 3.68 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, -0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.885, -3.85, -0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -3.85, -0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -4.45, -0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -4.45, -0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -3.85, 0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -3.85, 0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -4.45, 0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -4.45, 0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 3.85, 0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 3.85, 0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 4.45, 0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 4.45, 0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 3.85, -0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 3.85, -0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 4.45, -0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 4.45, -0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.84, -2.9, -1.84, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.84, 2.9, 1.84, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.84, 2.9, 1.84, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.84, -2.9, -1.84, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.92, -3.15, -1.92, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.92, 3.15, 1.92, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.92, 3.15, 1.92, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.92, -3.15, -1.92, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.19, -3.15, 1.92, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.92, -3.15, 1.92, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.92, 3.15, 1.19, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.19, -3.15, -1.92, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.92, -3.15, -1.92, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.92, 3.15, -1.19, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.12, -3.35, 2.12, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.12, 3.35, 1.21, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.21, 3.35, 1.21, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.21, 5.155, -1.21, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.21, 5.155, -1.21, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.21, 3.35, -2.12, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.12, 3.35, -2.12, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.12, -3.35, -1.21, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.21, -3.35, -1.21, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.21, -5.155, 1.21, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.21, -5.155, 1.21, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.21, -3.35, 2.12, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-2P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_3P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-3P');
        NewPcbLibComp.Name := 'SW1.27-3P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 6 pin, 4.95 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.27, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 1.27, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 1.27, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 0, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, -1.27, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.52, -3.85, -1.02, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, -3.85, -1.02, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, -4.45, -1.52, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, -4.45, -1.52, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, -3.85, 0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -3.85, 0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -4.45, -0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, -4.45, -0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, -3.85, 1.52, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, -3.85, 1.52, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, -4.45, 1.02, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, -4.45, 1.02, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, 3.85, 1.02, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, 3.85, 1.02, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, 4.45, 1.52, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, 4.45, 1.52, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 3.85, -0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 3.85, -0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 4.45, 0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 4.45, 0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, 3.85, -1.52, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, 3.85, -1.52, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, 4.45, -1.02, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, 4.45, -1.02, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.475, -2.9, -2.475, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.475, 2.9, 2.475, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.475, 2.9, 2.475, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.475, -2.9, -2.475, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.55, -3.15, -2.55, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.55, 3.15, 2.55, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.55, 3.15, 2.55, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.55, -3.15, -2.55, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.825, -3.15, 2.55, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.55, -3.15, 2.55, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.55, 3.15, 1.825, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.825, -3.15, -2.55, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.55, -3.15, -2.55, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.55, 3.15, -1.825, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.75, -3.35, 2.75, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.75, 3.35, 1.845, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.845, 3.35, 1.845, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.845, 5.155, -1.845, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.845, 5.155, -1.845, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.845, 3.35, -2.75, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.75, 3.35, -2.75, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.75, -3.35, -1.845, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.845, -3.35, -1.845, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.845, -5.155, 1.845, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.845, -5.155, 1.845, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.845, -3.35, 2.75, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-3P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_4P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-4P');
        NewPcbLibComp.Name := 'SW1.27-4P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 8 pin, 6.22 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, -0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, -1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.155, -3.85, -1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -3.85, -1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -4.45, -2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -4.45, -2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -3.85, -0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -3.85, -0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -4.45, -0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -4.45, -0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -3.85, 0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -3.85, 0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -4.45, 0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -4.45, 0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -3.85, 2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -3.85, 2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -4.45, 1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -4.45, 1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 3.85, 1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 3.85, 1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 4.45, 2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 4.45, 2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 3.85, 0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 3.85, 0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 4.45, 0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 4.45, 0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 3.85, -0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 3.85, -0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 4.45, -0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 4.45, -0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 3.85, -2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 3.85, -2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 4.45, -1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 4.45, -1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.11, -2.9, -3.11, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.11, 2.9, 3.11, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.11, 2.9, 3.11, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.11, -2.9, -3.11, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.19, -3.15, -3.19, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.19, 3.15, 3.19, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.19, 3.15, 3.19, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.19, -3.15, -3.19, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.46, -3.15, 3.19, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.19, -3.15, 3.19, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.19, 3.15, 2.46, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.46, -3.15, -3.19, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.19, -3.15, -3.19, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.19, 3.15, -2.46, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.39, -3.35, 3.39, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.39, 3.35, 2.48, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.48, 3.35, 2.48, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.48, 5.155, -2.48, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.48, 5.155, -2.48, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.48, 3.35, -3.39, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.39, 3.35, -3.39, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.39, -3.35, -2.48, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.48, -3.35, -2.48, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.48, -5.155, 2.48, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.48, -5.155, 2.48, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.48, -3.35, 3.39, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-4P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_5P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-5P');
        NewPcbLibComp.Name := 'SW1.27-5P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 10 pin, 7.49 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -2.54, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -1.27, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, 0, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 1.27, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 2.54, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 2.54, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, 1.27, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, 0, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '9', eTopLayer, -1.27, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '10', eTopLayer, -2.54, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.79, -3.85, -2.29, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.29, -3.85, -2.29, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.29, -4.45, -2.79, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.79, -4.45, -2.79, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, -3.85, -1.02, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, -3.85, -1.02, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, -4.45, -1.52, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, -4.45, -1.52, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, -3.85, 0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -3.85, 0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, -4.45, -0.25, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, -4.45, -0.25, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, -3.85, 1.52, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, -3.85, 1.52, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, -4.45, 1.02, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, -4.45, 1.02, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.29, -3.85, 2.79, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.79, -3.85, 2.79, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.79, -4.45, 2.29, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.29, -4.45, 2.29, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.79, 3.85, 2.29, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.29, 3.85, 2.29, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.29, 4.45, 2.79, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.79, 4.45, 2.79, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, 3.85, 1.02, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, 3.85, 1.02, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.02, 4.45, 1.52, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.52, 4.45, 1.52, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 3.85, -0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 3.85, -0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.25, 4.45, 0.25, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.25, 4.45, 0.25, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, 3.85, -1.52, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, 3.85, -1.52, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.52, 4.45, -1.02, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.02, 4.45, -1.02, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.29, 3.85, -2.79, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.79, 3.85, -2.79, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.79, 4.45, -2.29, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.29, 4.45, -2.29, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.745, -2.9, -3.745, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.745, 2.9, 3.745, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.745, 2.9, 3.745, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.745, -2.9, -3.745, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.82, -3.15, -3.82, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.82, 3.15, 3.82, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.82, 3.15, 3.82, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.82, -3.15, -3.82, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.095, -3.15, 3.82, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.82, -3.15, 3.82, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.82, 3.15, 3.095, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.095, -3.15, -3.82, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.82, -3.15, -3.82, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.82, 3.15, -3.095, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.02, -3.35, 4.02, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.02, 3.35, 3.115, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.115, 3.35, 3.115, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.115, 5.155, -3.115, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.115, 5.155, -3.115, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.115, 3.35, -4.02, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.02, 3.35, -4.02, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.02, -3.35, -3.115, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.115, -3.35, -3.115, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.115, -5.155, 3.115, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.115, -5.155, 3.115, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.115, -3.35, 4.02, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-5P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_6P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-6P');
        NewPcbLibComp.Name := 'SW1.27-6P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 12 pin, 8.76 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, -0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, 0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, 3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, 1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '9', eTopLayer, 0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '10', eTopLayer, -0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '11', eTopLayer, -1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '12', eTopLayer, -3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -3.425, -3.85, -2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -3.85, -2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -4.45, -3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, -4.45, -3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -3.85, -1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -3.85, -1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -4.45, -2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -4.45, -2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -3.85, -0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -3.85, -0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -4.45, -0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -4.45, -0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -3.85, 0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -3.85, 0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -4.45, 0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -4.45, 0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -3.85, 2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -3.85, 2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -4.45, 1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -4.45, 1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -3.85, 3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -3.85, 3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -4.45, 2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -4.45, 2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 3.85, 2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 3.85, 2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 4.45, 3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 4.45, 3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 3.85, 1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 3.85, 1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 4.45, 2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 4.45, 2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 3.85, 0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 3.85, 0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 4.45, 0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 4.45, 0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 3.85, -0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 3.85, -0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 4.45, -0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 4.45, -0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 3.85, -2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 3.85, -2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 4.45, -1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 4.45, -1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 3.85, -3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 3.85, -3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 4.45, -2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 4.45, -2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.38, -2.9, -4.38, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.38, 2.9, 4.38, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.38, 2.9, 4.38, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.38, -2.9, -4.38, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.46, -3.15, -4.46, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.46, 3.15, 4.46, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.46, 3.15, 4.46, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.46, -3.15, -4.46, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.73, -3.15, 4.46, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.46, -3.15, 4.46, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.46, 3.15, 3.73, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.73, -3.15, -4.46, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.46, -3.15, -4.46, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.46, 3.15, -3.73, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 4.66, -3.35, 4.66, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.66, 3.35, 3.75, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.75, 3.35, 3.75, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.75, 5.155, -3.75, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.75, 5.155, -3.75, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.75, 3.35, -4.66, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.66, 3.35, -4.66, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.66, -3.35, -3.75, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.75, -3.35, -3.75, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.75, -5.155, 3.75, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.75, -5.155, 3.75, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.75, -3.35, 4.66, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-6P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentSW1_27_8P(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('SW1.27-8P');
        NewPcbLibComp.Name := 'SW1.27-8P';
        NewPCBLibComp.Description := 'Ceramic Flat Pack (CFP), 1.27 mm pitch; 16 pin, 11.30 mm L X 5.80 mm W X 2.50 mm H body';
        NewPCBLibComp.Height := MMsToCoord(2.5);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -4.445, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, -1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, -0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, 0.635, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 1.905, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, 3.175, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, 4.445, -4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 270, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '9', eTopLayer, 4.445, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '10', eTopLayer, 3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '11', eTopLayer, 1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '12', eTopLayer, 0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '13', eTopLayer, -0.635, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '14', eTopLayer, -1.905, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '15', eTopLayer, -3.175, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '16', eTopLayer, -4.445, 4.09, 0, 0, eRoundedRectangular, 1.73, 0.75, 90, 50.67, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -4.695, -3.85, -4.195, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, -3.85, -4.195, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, -4.45, -4.695, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, -4.45, -4.695, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, -3.85, -2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -3.85, -2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, -4.45, -3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, -4.45, -3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -3.85, -1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -3.85, -1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, -4.45, -2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, -4.45, -2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -3.85, -0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -3.85, -0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, -4.45, -0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, -4.45, -0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -3.85, 0.885, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -3.85, 0.885, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, -4.45, 0.385, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, -4.45, 0.385, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -3.85, 2.155, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -3.85, 2.155, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, -4.45, 1.655, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, -4.45, 1.655, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -3.85, 3.425, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -3.85, 3.425, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, -4.45, 2.925, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, -4.45, 2.925, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, -3.85, 4.695, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, -3.85, 4.695, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, -4.45, 4.195, -4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, -4.45, 4.195, -3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, 3.85, 4.195, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, 3.85, 4.195, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.195, 4.45, 4.695, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.695, 4.45, 4.695, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 3.85, 2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 3.85, 2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.925, 4.45, 3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.425, 4.45, 3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 3.85, 1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 3.85, 1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.655, 4.45, 2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.155, 4.45, 2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 3.85, 0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 3.85, 0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.385, 4.45, 0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.885, 4.45, 0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 3.85, -0.885, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 3.85, -0.885, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.885, 4.45, -0.385, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.385, 4.45, -0.385, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 3.85, -2.155, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 3.85, -2.155, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.155, 4.45, -1.655, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.655, 4.45, -1.655, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 3.85, -3.425, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 3.85, -3.425, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.425, 4.45, -2.925, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.925, 4.45, -2.925, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, 3.85, -4.695, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, 3.85, -4.695, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.695, 4.45, -4.195, 4.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.195, 4.45, -4.195, 3.85, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.65, -2.9, -5.65, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.65, 2.9, 5.65, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.65, 2.9, 5.65, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.65, -2.9, -5.65, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.73, -3.15, -5.73, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.73, 3.15, 5.73, 3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.73, 3.15, 5.73, -3.15, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.73, -3.15, -5.73, -3.15, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5, -3.15, 5.73, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.73, -3.15, 5.73, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.73, 3.15, 5, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5, -3.15, -5.73, -3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.73, -3.15, -5.73, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -5.73, 3.15, -5, 3.15, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 5.93, -3.35, 5.93, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.93, 3.35, 5.02, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.02, 3.35, 5.02, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.02, 5.155, -5.02, 5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.02, 5.155, -5.02, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.02, 3.35, -5.93, 3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.93, 3.35, -5.93, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.93, -3.35, -5.02, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.02, -3.35, -5.02, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -5.02, -5.155, 5.02, -5.155, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.02, -5.155, 5.02, -3.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 5.02, -3.35, 5.93, -3.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Switch\SW1.27-8P.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;

Procedure CreateAPCBLibrary(Zero : integer);
Var
    View     : IServerDocumentView;
    Document : IServerDocument;

Begin
    If PCBServer = Nil Then
    Begin
        ShowMessage('No PCBServer present. This script inserts a footprint into an existing PCB Library that has the current focus.');
        Exit;
    End;

    CurrentLib := PcbServer.GetCurrentPCBLibrary;
    If CurrentLib = Nil Then
    Begin
        ShowMessage('You must have focus on a PCB Library in order for this script to run.');
        Exit;
    End;

    View := Client.GetCurrentView;
    Document := View.OwnerDocument;
    Document.Modified := True;


		CreateComponentSW1_27_10P(0);
		CreateComponentSW1_27_1P(0);
		CreateComponentSW1_27_2P(0);
		CreateComponentSW1_27_3P(0);
		CreateComponentSW1_27_4P(0);
		CreateComponentSW1_27_5P(0);
		CreateComponentSW1_27_6P(0);
		CreateComponentSW1_27_8P(0);
    
End;

Procedure CreateALibrary;
Begin
    Screen.Cursor := crHourGlass;

    CreateAPCBLibrary(0);

    Screen.Cursor := crArrow;
End;

End.