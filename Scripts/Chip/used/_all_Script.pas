Var
    CurrentSCHLib : ISch_Lib;
    CurrentLib : IPCB_Library;

Function CreateAComponent(Name: String) : IPCB_LibComponent;
Var
    PrimitiveList: TInterfaceList;
    PrimitiveIterator: IPCB_GroupIterator;
    PrimitiveHandle: IPCB_Primitive;
    I:  Integer;

Begin
    // Check if footprint already in library
    Result := CurrentLib.GetComponentByName(Name);
    If Result = Nil Then
    Begin
        // Create New Component
        Result := PCBServer.CreatePCBLibComp;
        Result.Name := Name;
    End
    Else
    Begin
        // Clear existin component
        Try
            // Create List with all primitives on board
            PrimitiveList := TInterfaceList.Create;
            PrimitiveIterator := Result.GroupIterator_Create;
            PrimitiveIterator.AddFilter_ObjectSet(AllObjects);
            PrimitiveHandle := PrimitiveIterator.FirstPCBObject;
            While PrimitiveHandle <> Nil Do
            Begin
                PrimitiveList.Add(PrimitiveHandle);
                PrimitiveHandle := PrimitiveIterator.NextPCBObject;
            End;

            // Delete all primitives
            For I := 0 To PrimitiveList.Count - 1 Do
            Begin
                PrimitiveHandle := PrimitiveList.items[i];
                Result.RemovePCBObject(PrimitiveHandle);
                Result.GraphicallyInvalidate;
            End;

        Finally
            Result.GroupIterator_Destroy(PrimitiveIterator);
            PrimitiveList.Free;
        End;
    End;
End; 


Procedure CreateTHComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, HoleType : TExtendedHoleType,
                               HoleSize : Real, HoleLength : Real, Layer : TLayer, X : Real, Y : Real,
                               OffsetX : Real, OffsetY : Real, TopShape : TShape, TopXSize : Real, TopYSize : Real,
                               InnerShape : TShape, InnerXSize : Real, InnerYSize : Real,
                               BottomShape : TShape, BottomXSize : Real, BottomYSize : Real,
                               Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion: Real, Plated : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleType := HoleType;
    NewPad.HoleSize := MMsToCoord(HoleSize);
    if HoleLength <> 0 then
        NewPad.HoleWidth := MMsToCoord(HoleLength);
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    if BottomShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eBottomLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.MidShape := InnerShape;
    NewPad.MidXSize := MMsToCoord(InnerXSize);
    NewPad.MidYSize := MMsToCoord(InnerYSize);
    NewPad.BotShape := BottomShape;
    NewPad.BotXSize := MMsToCoord(BottomXSize);
    NewPad.BotYSize := MMsToCoord(BottomYSize);
    NewPad.SetState_XPadOffsetOnLayer(Layer, MMsToCoord(OffsetX));
    NewPad.SetState_YPadOffsetOnLayer(Layer, MMsToCoord(OffsetY));
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Plated   := Plated;
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if PMExpansion <> 0 then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if SMExpansion <> 0 then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateSMDComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, Layer : TLayer, X : Real, Y : Real, OffsetX : Real, OffsetY : Real,
                                TopShape : TShape, TopXSize : Real, TopYSize : Real, Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion : Real,
                                PMFromRules : Boolean, SMFromRules : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleSize := MMsToCoord(0);
    NewPad.Layer    := Layer;
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if (PMExpansion <> 0) or (PMFromRules = False) then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if (SMExpansion <> 0) or (SMFromRules = False) then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateComponentTrack(NewPCBLibComp : IPCB_LibComponent, X1 : Real, Y1 : Real, X2 : Real, Y2 : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewTrack                    : IPCB_Track;

Begin
    NewTrack := PcbServer.PCBObjectFactory(eTrackObject,eNoDimension,eCreate_Default);
    NewTrack.X1 := MMsToCoord(X1);
    NewTrack.Y1 := MMsToCoord(Y1);
    NewTrack.X2 := MMsToCoord(X2);
    NewTrack.Y2 := MMsToCoord(Y2);
    NewTrack.Layer := Layer;
    NewTrack.Width := MMsToCoord(LineWidth);
    NewTrack.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewTrack);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewTrack.I_ObjectAddress);
End;

Procedure CreateComponentArc(NewPCBLibComp : IPCB_LibComponent, CenterX : Real, CenterY : Real, Radius : Real, StartAngle : Real, EndAngle : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewArc                      : IPCB_Arc;

Begin
    NewArc := PCBServer.PCBObjectFactory(eArcObject,eNoDimension,eCreate_Default);
    NewArc.XCenter := MMsToCoord(CenterX);
    NewArc.YCenter := MMsToCoord(CenterY);
    NewArc.Radius := MMsToCoord(Radius);
    NewArc.StartAngle := StartAngle;
    NewArc.EndAngle := EndAngle;
    NewArc.Layer := Layer;
    NewArc.LineWidth := MMsToCoord(LineWidth);
    NewArc.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewArc);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewArc.I_ObjectAddress);
End;

Function ReadStringFromIniFile(Section: String, Name: String, FilePath: String, IfEmpty: String) : String;
Var
    IniFile                     : TIniFile;

Begin
    result := IfEmpty;
    If FileExists(FilePath) Then
    Begin
        Try
            IniFile := TIniFile.Create(FilePath);

            Result := IniFile.ReadString(Section, Name, IfEmpty);
        Finally
            Inifile.Free;
        End;
    End;
End;

Procedure EnableMechanicalLayers(Zero : Integer);
Var
    Board                       : IPCB_Board;
    MajorADVersion              : Integer;

Begin
End;


Procedure CreateComponentC0201(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C0201';
        NewPCBLibComp.Description := 'Capacitor, Chip; 0.60 mm L X 0.30 mm W X 0.28 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.28);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 180, 48.28, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 0, 48.28, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.2, 0.15, -0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.2, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, -0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, -0.15, 0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, 0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, -0.18, -0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, 0.18, 0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, 0.18, 0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, -0.18, -0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, -0.33, -0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, 0.33, 0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, 0.33, 0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, -0.33, -0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1238, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.165, 0, -0.165, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.165, 0, -0.165, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C0201.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC0402(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C0402';
        NewPCBLibComp.Description := 'Capacitor, Chip; 1.00 mm L X 0.50 mm W X 0.31 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.31);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 180, 50.91, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 0, 50.91, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.3, 0.25, -0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, -0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.25, 0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, 0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, -0.3, -0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, 0.3, 0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, 0.3, 0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, -0.3, -0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, -0.46, -0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, 0.46, 0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, 0.46, 0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, -0.46, -0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1725, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.23, 0, -0.23, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.23, 0, -0.23, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C0402.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC0603(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C0603';
        NewPCBLibComp.Description := 'Capacitor, Chip; 1.60 mm L X 0.80 mm W X 0.41 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.41);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 180, 50.53, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 0, 50.53, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.4, 0.4, -0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.4, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, -0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, -0.4, 0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, 0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, -0.48, -0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, 0.48, 0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, 0.48, 0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, -0.48, -0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, -0.69, -1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, 0.69, 1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, 0.69, 1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, -0.69, -1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.345, 0, -0.345, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.345, 0, -0.345, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C0603.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC0805(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C0805';
        NewPCBLibComp.Description := 'Capacitor, Chip; 2.00 mm L X 1.20 mm W X 0.60 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.6);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 180, 45.87, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 0, 45.87, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.5, 0.6, -0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, -0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.6, 0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, 0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.68, -1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, 0.68, 1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.68, 1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.68, -1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, 0.68, 0.14, 0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, -0.68, 0.14, -0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, -0.89, -1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, 0.89, 1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, 0.89, 1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, -0.89, -1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C0805.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC1206(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C1206';
        NewPCBLibComp.Description := 'Capacitor, Chip; 3.20 mm L X 1.60 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 180, 43.86, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 0, 43.86, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.1, 0.8, -1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, -1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.8, 1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, 1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, -0.88, -1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, 0.88, 1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, 0.88, 1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, -0.88, -1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, 0.88, 0.74, 0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, -0.88, 0.74, -0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, -1.09, -2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, 1.09, 2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, 1.09, 2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, -1.09, -2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C1206.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC2010(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C2010';
        NewPCBLibComp.Description := 'Capacitor, Chip; 5.00 mm L X 2.50 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -2.415, 0, 0, 0, eRoundedRectangular, 1.39, 2.72, 180, 35.97, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 2.415, 0, 0, 0, eRoundedRectangular, 1.39, 2.72, 0, 35.97, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.9, 1.25, -1.9, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -1.25, -2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, -1.25, -2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, 1.25, -1.9, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -1.25, 1.9, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 1.25, 2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, 1.25, 2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, -1.25, 1.9, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, -1.25, -2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, 1.25, 2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, 1.25, 2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, -1.25, -2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, -1.35, -2.6, 1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, 1.35, 2.6, 1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, 1.35, 2.6, -1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, -1.35, -2.6, -1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, 1.35, 1.54, 1.35, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, -1.35, 1.54, -1.35, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.31, -1.56, -3.31, 1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.31, 1.56, 3.31, 1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.31, 1.56, 3.31, -1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.31, -1.56, -3.31, -1.56, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C2010.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentC2512(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'C2512';
        NewPCBLibComp.Description := 'Capacitor, Chip; 6.40 mm L X 3.20 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 180, 35.97, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 0, 35.97, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.6, 1.6, -2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, -2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, -1.6, 2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, 2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, -1.7, -3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, 1.7, 3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, 1.7, 3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, -1.7, -3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, 1.7, 2.24, 1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, -1.7, 2.24, -1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, -1.91, -4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, 1.91, 4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, 1.91, 4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, -1.91, -4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\C2512.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL0201(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L0201';
        NewPCBLibComp.Description := 'Inductor, Chip; 0.60 mm L X 0.30 mm W X 0.28 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.28);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 180, 48.28, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 0, 48.28, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.2, 0.15, -0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.2, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, -0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, -0.15, 0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, 0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, -0.18, -0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, 0.18, 0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, 0.18, 0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, -0.18, -0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, -0.33, -0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, 0.33, 0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, 0.33, 0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, -0.33, -0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1238, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.165, 0, -0.165, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.165, 0, -0.165, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L0201.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL0402(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L0402';
        NewPCBLibComp.Description := 'Inductor, Chip; 1.00 mm L X 0.50 mm W X 0.31 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.31);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 180, 50.91, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 0, 50.91, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.3, 0.25, -0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, -0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.25, 0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, 0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, -0.3, -0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, 0.3, 0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, 0.3, 0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, -0.3, -0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, -0.46, -0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, 0.46, 0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, 0.46, 0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, -0.46, -0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1725, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.23, 0, -0.23, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.23, 0, -0.23, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L0402.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL0603(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L0603';
        NewPCBLibComp.Description := 'Inductor, Chip; 1.60 mm L X 0.80 mm W X 0.41 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.41);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 180, 50.53, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 0, 50.53, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.4, 0.4, -0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.4, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, -0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, -0.4, 0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, 0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, -0.48, -0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, 0.48, 0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, 0.48, 0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, -0.48, -0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, -0.69, -1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, 0.69, 1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, 0.69, 1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, -0.69, -1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.345, 0, -0.345, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.345, 0, -0.345, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L0603.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL0805(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L0805';
        NewPCBLibComp.Description := 'Inductor, Chip; 2.00 mm L X 1.20 mm W X 0.60 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.6);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 180, 45.87, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 0, 45.87, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.5, 0.6, -0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, -0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.6, 0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, 0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.68, -1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, 0.68, 1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.68, 1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.68, -1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, 0.68, 0.14, 0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, -0.68, 0.14, -0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, -0.89, -1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, 0.89, 1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, 0.89, 1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, -0.89, -1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L0805.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL1206(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L1206';
        NewPCBLibComp.Description := 'Inductor, Chip; 3.20 mm L X 1.60 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 180, 43.86, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 0, 43.86, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.1, 0.8, -1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, -1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.8, 1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, 1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, -0.88, -1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, 0.88, 1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, 0.88, 1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, -0.88, -1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, 0.88, 0.74, 0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, -0.88, 0.74, -0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, -1.09, -2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, 1.09, 2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, 1.09, 2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, -1.09, -2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L1206.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentL2512(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'L2512';
        NewPCBLibComp.Description := 'Inductor, Chip; 6.40 mm L X 3.20 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 180, 35.97, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 0, 35.97, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.6, 1.6, -2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, -2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, -1.6, 2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, 2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, -1.7, -3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, 1.7, 3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, 1.7, 3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, -1.7, -3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, 1.7, 2.24, 1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, -1.7, 2.24, -1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, -1.91, -4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, 1.91, 4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, 1.91, 4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, -1.91, -4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\L2512.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR0201(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R0201';
        NewPCBLibComp.Description := 'Resistor, Chip; 0.60 mm L X 0.30 mm W X 0.28 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.28);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 180, 48.28, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.305, 0, 0, 0, eRoundedRectangular, 0.29, 0.36, 0, 48.28, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.2, 0.15, -0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.2, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, -0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, -0.15, 0.2, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.2, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, 0.2, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.15, -0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, 0.15, 0.3, 0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.15, 0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.15, -0.3, -0.15, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, -0.18, -0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.33, 0.18, 0.33, 0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, 0.18, 0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.33, -0.18, -0.33, -0.18, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, -0.33, -0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.6, 0.33, 0.6, 0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, 0.33, 0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.6, -0.33, -0.6, -0.33, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1238, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.165, 0, -0.165, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.165, 0, -0.165, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R0201.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR0402(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R0402';
        NewPCBLibComp.Description := 'Resistor, Chip; 1.00 mm L X 0.50 mm W X 0.31 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.31);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 180, 50.91, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.49, 0, 0, 0, eRoundedRectangular, 0.55, 0.61, 0, 50.91, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.3, 0.25, -0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.3, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, -0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, -0.25, 0.3, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.3, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, 0.3, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.25, -0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, 0.25, 0.5, 0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.25, 0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.25, -0.5, -0.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, -0.3, -0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.55, 0.3, 0.55, 0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, 0.3, 0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.55, -0.3, -0.55, -0.3, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, -0.46, -0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.92, 0.46, 0.92, 0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, 0.46, 0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.92, -0.46, -0.92, -0.46, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.1725, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.23, 0, -0.23, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.23, 0, -0.23, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R0402.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR0603(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R0603';
        NewPCBLibComp.Description := 'Resistor, Chip; 1.60 mm L X 0.80 mm W X 0.41 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.41);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 180, 50.53, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.71, 0, 0, 0, eRoundedRectangular, 0.95, 0.97, 0, 50.53, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.4, 0.4, -0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.4, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, -0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, -0.4, 0.4, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.4, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, 0.4, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, -0.4, -0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.8, 0.4, 0.8, 0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, 0.4, 0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.8, -0.4, -0.8, -0.4, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, -0.48, -0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.88, 0.48, 0.88, 0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, 0.48, 0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 0.88, -0.48, -0.88, -0.48, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, -0.69, -1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.39, 0.69, 1.39, 0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, 0.69, 1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.39, -0.69, -1.39, -0.69, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.345, 0, -0.345, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.345, 0, -0.345, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R0603.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR0805(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R0805';
        NewPCBLibComp.Description := 'Resistor, Chip; 2.00 mm L X 1.20 mm W X 0.60 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.6);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 180, 45.87, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 0.865, 0, 0, 0, eRoundedRectangular, 1.09, 1.37, 0, 45.87, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -0.5, 0.6, -0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.5, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, -0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, -0.6, 0.5, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.5, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, 0.5, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, -0.6, -1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1, 0.6, 1, 0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, 0.6, 1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1, -0.6, -1, -0.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.68, -1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, 0.68, 1.1, 0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.68, 1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.68, -1.1, -0.68, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, 0.68, 0.14, 0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.14, -0.68, 0.14, -0.68, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, -0.89, -1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -1.61, 0.89, 1.61, 0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, 0.89, 1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 1.61, -0.89, -1.61, -0.89, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R0805.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR1206(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R1206';
        NewPCBLibComp.Description := 'Resistor, Chip; 3.20 mm L X 1.60 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 180, 43.86, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 1.49, 0, 0, 0, eRoundedRectangular, 1.14, 1.77, 0, 43.86, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.1, 0.8, -1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.1, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, -1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, -0.8, 1.1, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.1, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, 1.1, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, -0.8, -1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.6, 0.8, 1.6, 0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, 0.8, 1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.6, -0.8, -1.6, -0.8, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, -0.88, -1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.7, 0.88, 1.7, 0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, 0.88, 1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 1.7, -0.88, -1.7, -0.88, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, 0.88, 0.74, 0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -0.74, -0.88, 0.74, -0.88, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, -1.09, -2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -2.26, 1.09, 2.26, 1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, 1.09, 2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 2.26, -1.09, -2.26, -1.09, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R1206.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR1812(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R1812';
        NewPCBLibComp.Description := 'Resistor, Chip; 4.80 mm L X 3.20 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -2.365, 0, 0, 0, eRoundedRectangular, 1.29, 3.42, 180, 38.76, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 2.365, 0, 0, 0, eRoundedRectangular, 1.29, 3.42, 0, 38.76, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.9, 1.6, -1.9, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -1.6, -2.4, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.4, -1.6, -2.4, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.4, 1.6, -1.9, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -1.6, 1.9, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 1.6, 2.4, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.4, 1.6, 2.4, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.4, -1.6, 1.9, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.4, -1.6, -2.4, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.4, 1.6, 2.4, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.4, 1.6, 2.4, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.4, -1.6, -2.4, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, -1.7, -2.5, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, 1.7, 2.5, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, 1.7, 2.5, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, -1.7, -2.5, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, 1.7, 1.54, 1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, -1.7, 1.54, -1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.21, -1.91, -3.21, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.21, 1.91, 3.21, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.21, 1.91, 3.21, -1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.21, -1.91, -3.21, -1.91, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R1812.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR2010(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R2010';
        NewPCBLibComp.Description := 'Resistor, Chip; 5.00 mm L X 2.50 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -2.415, 0, 0, 0, eRoundedRectangular, 1.39, 2.72, 180, 35.97, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 2.415, 0, 0, 0, eRoundedRectangular, 1.39, 2.72, 0, 35.97, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -1.9, 1.25, -1.9, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.9, -1.25, -2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, -1.25, -2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, 1.25, -1.9, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, -1.25, 1.9, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.9, 1.25, 2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, 1.25, 2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, -1.25, 1.9, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, -1.25, -2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.5, 1.25, 2.5, 1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, 1.25, 2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.5, -1.25, -2.5, -1.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, -1.35, -2.6, 1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, 1.35, 2.6, 1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, 1.35, 2.6, -1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, -1.35, -2.6, -1.35, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, 1.35, 1.54, 1.35, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -1.54, -1.35, 1.54, -1.35, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.31, -1.56, -3.31, 1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -3.31, 1.56, 3.31, 1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.31, 1.56, 3.31, -1.56, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 3.31, -1.56, -3.31, -1.56, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R2010.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;


Procedure CreateComponentR2512(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPCBLibComp := PCBServer.CreatePCBLibComp;
        NewPcbLibComp.Name := 'R2512';
        NewPCBLibComp.Description := 'Resistor, Chip; 6.40 mm L X 3.20 mm W X 0.65 mm H body';
        NewPCBLibComp.Height := MMsToCoord(0.65);

        

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 180, 35.97, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, 3.115, 0, 0, 0, eRoundedRectangular, 1.39, 3.42, 0, 35.97, 0, 0, True, True);

        CreateComponentTrack(NewPCBLibComp, -2.6, 1.6, -2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.6, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, -2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, -1.6, 2.6, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.6, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, 2.6, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, -1.6, -3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.2, 1.6, 3.2, 1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, 1.6, 3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.2, -1.6, -3.2, -1.6, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, -1.7, -3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -3.3, 1.7, 3.3, 1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, 1.7, 3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 3.3, -1.7, -3.3, -1.7, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, 1.7, 2.24, 1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -2.24, -1.7, 2.24, -1.7, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, -1.91, -4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -4.01, 1.91, 4.01, 1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, 1.91, 4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 4.01, -1.91, -4.01, -1.91, eMechanical15, 0.05, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0.35, 0, -0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\Chip\R2512.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;

Procedure CreateAPCBLibrary(Zero : integer);
Var
    View     : IServerDocumentView;
    Document : IServerDocument;

Begin
    If PCBServer = Nil Then
    Begin
        ShowMessage('No PCBServer present. This script inserts a footprint into an existing PCB Library that has the current focus.');
        Exit;
    End;

    CurrentLib := PcbServer.GetCurrentPCBLibrary;
    If CurrentLib = Nil Then
    Begin
        ShowMessage('You must have focus on a PCB Library in order for this script to run.');
        Exit;
    End;

    View := Client.GetCurrentView;
    Document := View.OwnerDocument;
    Document.Modified := True;


		CreateComponentC0201(0);
		CreateComponentC0402(0);
		CreateComponentC0603(0);
		CreateComponentC0805(0);
		CreateComponentC1206(0);
		CreateComponentC2010(0);
		CreateComponentC2512(0);
		CreateComponentL0201(0);
		CreateComponentL0402(0);
		CreateComponentL0603(0);
		CreateComponentL0805(0);
		CreateComponentL1206(0);
		CreateComponentL2512(0);
		CreateComponentR0201(0);
		CreateComponentR0402(0);
		CreateComponentR0603(0);
		CreateComponentR0805(0);
		CreateComponentR1206(0);
		CreateComponentR1812(0);
		CreateComponentR2010(0);
		CreateComponentR2512(0);
    
End;

Procedure CreateALibrary;
Begin
    Screen.Cursor := crHourGlass;

    CreateAPCBLibrary(0);

    Screen.Cursor := crArrow;
End;

End.