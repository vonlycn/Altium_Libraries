Var
    CurrentSCHLib : ISch_Lib;
    CurrentLib : IPCB_Library;

Function CreateAComponent(Name: String) : IPCB_LibComponent;
Var
    PrimitiveList: TInterfaceList;
    PrimitiveIterator: IPCB_GroupIterator;
    PrimitiveHandle: IPCB_Primitive;
    I:  Integer;

Begin
    // Check if footprint already in library
    Result := CurrentLib.GetComponentByName(Name);
    If Result = Nil Then
    Begin
        // Create New Component
        Result := PCBServer.CreatePCBLibComp;
        Result.Name := Name;
    End
    Else
    Begin
        // Clear existin component
        Try
            // Create List with all primitives on board
            PrimitiveList := TInterfaceList.Create;
            PrimitiveIterator := Result.GroupIterator_Create;
            PrimitiveIterator.AddFilter_ObjectSet(AllObjects);
            PrimitiveHandle := PrimitiveIterator.FirstPCBObject;
            While PrimitiveHandle <> Nil Do
            Begin
                PrimitiveList.Add(PrimitiveHandle);
                PrimitiveHandle := PrimitiveIterator.NextPCBObject;
            End;

            // Delete all primitives
            For I := 0 To PrimitiveList.Count - 1 Do
            Begin
                PrimitiveHandle := PrimitiveList.items[i];
                Result.RemovePCBObject(PrimitiveHandle);
                Result.GraphicallyInvalidate;
            End;

        Finally
            Result.GroupIterator_Destroy(PrimitiveIterator);
            PrimitiveList.Free;
        End;
    End;
End; 

Procedure CreateSMDComponentPad(NewPCBLibComp : IPCB_LibComponent, Name : String, Layer : TLayer, X : Real, Y : Real, OffsetX : Real, OffsetY : Real,
                                TopShape : TShape, TopXSize : Real, TopYSize : Real, Rotation: Real, CRRatio : Real, PMExpansion : Real, SMExpansion : Real,
                                PMFromRules : Boolean, SMFromRules : Boolean);
Var
    NewPad                      : IPCB_Pad2;
    PadCache                    : TPadCache;

Begin
    NewPad := PcbServer.PCBObjectFactory(ePadObject, eNoDimension, eCreate_Default);
    NewPad.HoleSize := MMsToCoord(0);
    NewPad.Layer    := Layer;
    NewPad.TopShape := TopShape;
    if TopShape = eRoundedRectangular then
        NewPad.SetState_StackCRPctOnLayer(eTopLayer, CRRatio);
    NewPad.TopXSize := MMsToCoord(TopXSize);
    NewPad.TopYSize := MMsToCoord(TopYSize);
    NewPad.RotateBy(Rotation);
    NewPad.MoveToXY(MMsToCoord(X), MMsToCoord(Y));
    NewPad.Name := Name;

    Padcache := NewPad.GetState_Cache;
    if (PMExpansion <> 0) or (PMFromRules = False) then
    Begin
        Padcache.PasteMaskExpansionValid   := eCacheManual;
        Padcache.PasteMaskExpansion        := MMsToCoord(PMExpansion);
    End;
    if (SMExpansion <> 0) or (SMFromRules = False) then
    Begin
        Padcache.SolderMaskExpansionValid  := eCacheManual;
        Padcache.SolderMaskExpansion       := MMsToCoord(SMExpansion);
    End;
    NewPad.SetState_Cache              := Padcache;

    NewPCBLibComp.AddPCBObject(NewPad);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewPad.I_ObjectAddress);
End;

Procedure CreateComponentTrack(NewPCBLibComp : IPCB_LibComponent, X1 : Real, Y1 : Real, X2 : Real, Y2 : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewTrack                    : IPCB_Track;

Begin
    NewTrack := PcbServer.PCBObjectFactory(eTrackObject,eNoDimension,eCreate_Default);
    NewTrack.X1 := MMsToCoord(X1);
    NewTrack.Y1 := MMsToCoord(Y1);
    NewTrack.X2 := MMsToCoord(X2);
    NewTrack.Y2 := MMsToCoord(Y2);
    NewTrack.Layer := Layer;
    NewTrack.Width := MMsToCoord(LineWidth);
    NewTrack.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewTrack);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewTrack.I_ObjectAddress);
End;

Procedure CreateComponentArc(NewPCBLibComp : IPCB_LibComponent, CenterX : Real, CenterY : Real, Radius : Real, StartAngle : Real, EndAngle : Real, Layer : TLayer, LineWidth : Real, IsKeepout : Boolean);
Var
    NewArc                      : IPCB_Arc;

Begin
    NewArc := PCBServer.PCBObjectFactory(eArcObject,eNoDimension,eCreate_Default);
    NewArc.XCenter := MMsToCoord(CenterX);
    NewArc.YCenter := MMsToCoord(CenterY);
    NewArc.Radius := MMsToCoord(Radius);
    NewArc.StartAngle := StartAngle;
    NewArc.EndAngle := EndAngle;
    NewArc.Layer := Layer;
    NewArc.LineWidth := MMsToCoord(LineWidth);
    NewArc.IsKeepout := IsKeepout;
    NewPCBLibComp.AddPCBObject(NewArc);
    PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,NewArc.I_ObjectAddress);
End;

Function ReadStringFromIniFile(Section: String, Name: String, FilePath: String, IfEmpty: String) : String;
Var
    IniFile                     : TIniFile;

Begin
    result := IfEmpty;
    If FileExists(FilePath) Then
    Begin
        Try
            IniFile := TIniFile.Create(FilePath);

            Result := IniFile.ReadString(Section, Name, IfEmpty);
        Finally
            Inifile.Free;
        End;
    End;
End;

Procedure EnableMechanicalLayers(Zero : Integer);
Var
    Board                       : IPCB_Board;
    MajorADVersion              : Integer;

Begin
End;

Procedure CreateComponentPowerSO20(Zero : integer);
Var
    NewPCBLibComp               : IPCB_LibComponent;
    NewPad                      : IPCB_Pad2;
    NewRegion                   : IPCB_Region;
    NewContour                  : IPCB_Contour;
    STEPmodel                   : IPCB_ComponentBody;
    Model                       : IPCB_Model;
    TextObj                     : IPCB_Text;

Begin
    Try
        PCBServer.PreProcess;

        EnableMechanicalLayers(0);

        NewPcbLibComp := CreateAComponent('PowerSO20');
        NewPcbLibComp.Name := 'PowerSO20';
        NewPCBLibComp.Description := 'Small Outline IC (SOIC with Tab), 1.27 mm pitch; 20 pin, 15.80 mm L X 10.90 mm W X 3.60 mm H body';
        NewPCBLibComp.Height := MMsToCoord(3.6);

        // Create text object for .Designator
        TextObj := PCBServer.PCBObjectFactory(eTextObject, eNoDimension, eCreate_Default);
        TextObj.UseTTFonts := True;
        TextObj.Layer := eMechanical16;
        TextObj.Text := '.Designator';
        TextObj.Size := MMsToCoord(1.2);
        NewPCBLibComp.AddPCBObject(TextObj);
        PCBServer.SendMessageToRobots(NewPCBLibComp.I_ObjectAddress,c_Broadcast,PCBM_BoardRegisteration,TextObj.I_ObjectAddress);

        CreateSMDComponentPad(NewPCBLibComp, '1', eTopLayer, -5.715, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '2', eTopLayer, -4.445, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '3', eTopLayer, -3.175, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '4', eTopLayer, -1.905, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '5', eTopLayer, -0.635, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '6', eTopLayer, 0.635, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '7', eTopLayer, 1.905, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '8', eTopLayer, 3.175, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '9', eTopLayer, 4.445, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '10', eTopLayer, 5.715, -6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 270, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '11', eTopLayer, 5.715, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '12', eTopLayer, 4.445, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '13', eTopLayer, 3.175, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '14', eTopLayer, 1.905, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '15', eTopLayer, 0.635, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '16', eTopLayer, -0.635, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '17', eTopLayer, -1.905, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '18', eTopLayer, -3.175, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '19', eTopLayer, -4.445, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '20', eTopLayer, -5.715, 6.78, 0, 0, eRoundedRectangular, 1.86, 0.62, 90, 51.61, 0, 0, True, True);
        CreateSMDComponentPad(NewPCBLibComp, '21', eTopLayer, 0, 0, 0, 0, eRectangular, 5.8, 13.8, 90, 0, -13.8, -13.8, True, True);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(-2.9));
        NewContour.AddPoint(MMsToCoord(-6.9), MMsToCoord(-2.9));
        NewContour.AddPoint(MMsToCoord(-7.9), MMsToCoord(-1.9));
        NewContour.AddPoint(MMsToCoord(-7.9), MMsToCoord(2.9));
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(2.9));
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(-2.9));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopLayer;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(-2.9));
        NewContour.AddPoint(MMsToCoord(-6.9), MMsToCoord(-2.9));
        NewContour.AddPoint(MMsToCoord(-7.9), MMsToCoord(-1.9));
        NewContour.AddPoint(MMsToCoord(-7.9), MMsToCoord(2.9));
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(2.9));
        NewContour.AddPoint(MMsToCoord(7.9), MMsToCoord(-2.9));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopSolder;
        NewPCBLibComp.AddPCBObject(NewRegion);


        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(6.59), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(6.59), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(6.59), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(6.59), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(7.81), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(5.15), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(5.15), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(5.15), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(5.15), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(6.37), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(3.71), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(3.71), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(3.71), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(3.71), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(4.93), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(2.27), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(2.27), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(2.27), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(2.27), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(3.49), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(0.83), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(0.83), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(0.83), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(0.83), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(2.05), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-0.61), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-0.61), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-0.61), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-0.61), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(0.61), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-2.05), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-2.05), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-2.05), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-2.05), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-0.83), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-3.49), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-3.49), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-3.49), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-3.49), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-2.27), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-4.93), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-4.93), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-4.93), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-4.93), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-3.71), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-6.37), MMsToCoord(-2.305));
        NewContour.AddPoint(MMsToCoord(-6.37), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(-2.305));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-6.37), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-6.37), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-5.15), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(-2.208));
        NewContour.AddPoint(MMsToCoord(-7.592), MMsToCoord(-2.208));
        NewContour.AddPoint(MMsToCoord(-7.592), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(-0.595));
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(-2.208));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        NewRegion := PCBServer.PCBObjectFactory(eRegionObject, eNoDimension, eCreate_Default);
        NewContour := PCBServer.PCBContourFactory;
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-7.81), MMsToCoord(0.595));
        NewContour.AddPoint(MMsToCoord(-7.81), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(2.305));
        NewContour.AddPoint(MMsToCoord(-6.59), MMsToCoord(0.595));
        NewRegion.SetOutlineContour(NewContour);
        NewRegion.Layer := eTopPaste;
        NewPCBLibComp.AddPCBObject(NewRegion);

        CreateComponentTrack(NewPCBLibComp, -5.915, -6.45, -5.515, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.515, -6.45, -5.515, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.515, -7.25, -5.915, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.915, -7.25, -5.915, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.645, -6.45, -4.245, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.245, -6.45, -4.245, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.245, -7.25, -4.645, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.645, -7.25, -4.645, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.375, -6.45, -2.975, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.975, -6.45, -2.975, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.975, -7.25, -3.375, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.375, -7.25, -3.375, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.105, -6.45, -1.705, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.705, -6.45, -1.705, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.705, -7.25, -2.105, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.105, -7.25, -2.105, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.835, -6.45, -0.435, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.435, -6.45, -0.435, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.435, -7.25, -0.835, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.835, -7.25, -0.835, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.435, -6.45, 0.835, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.835, -6.45, 0.835, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.835, -7.25, 0.435, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.435, -7.25, 0.435, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.705, -6.45, 2.105, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.105, -6.45, 2.105, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.105, -7.25, 1.705, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.705, -7.25, 1.705, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.975, -6.45, 3.375, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.375, -6.45, 3.375, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.375, -7.25, 2.975, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.975, -7.25, 2.975, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.245, -6.45, 4.645, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.645, -6.45, 4.645, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.645, -7.25, 4.245, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.245, -7.25, 4.245, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.515, -6.45, 5.915, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.915, -6.45, 5.915, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.915, -7.25, 5.515, -7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.515, -7.25, 5.515, -6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.915, 6.45, 5.515, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.515, 6.45, 5.515, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.515, 7.25, 5.915, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 5.915, 7.25, 5.915, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.645, 6.45, 4.245, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.245, 6.45, 4.245, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.245, 7.25, 4.645, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 4.645, 7.25, 4.645, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.375, 6.45, 2.975, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.975, 6.45, 2.975, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.975, 7.25, 3.375, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 3.375, 7.25, 3.375, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.105, 6.45, 1.705, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.705, 6.45, 1.705, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 1.705, 7.25, 2.105, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 2.105, 7.25, 2.105, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.835, 6.45, 0.435, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.435, 6.45, 0.435, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.435, 7.25, 0.835, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 0.835, 7.25, 0.835, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.435, 6.45, -0.835, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.835, 6.45, -0.835, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.835, 7.25, -0.435, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -0.435, 7.25, -0.435, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.705, 6.45, -2.105, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.105, 6.45, -2.105, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.105, 7.25, -1.705, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -1.705, 7.25, -1.705, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.975, 6.45, -3.375, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.375, 6.45, -3.375, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -3.375, 7.25, -2.975, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -2.975, 7.25, -2.975, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.245, 6.45, -4.645, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.645, 6.45, -4.645, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.645, 7.25, -4.245, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -4.245, 7.25, -4.245, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.515, 6.45, -5.915, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.915, 6.45, -5.915, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.915, 7.25, -5.515, 7.25, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -5.515, 7.25, -5.515, 6.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -6.9, -2.9, 7.9, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 7.9, -2.9, 7.9, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 7.9, 2.9, -7.9, 2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7.9, 2.9, -7.9, -1.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7.9, -1.9, -6.9, -2.9, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7.9, -5.45, -7.9, 5.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7.9, 5.45, 7.9, 5.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 7.9, 5.45, 7.9, -5.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, 7.9, -5.45, -7.9, -5.45, eMechanical12, 0.025, False);
        CreateComponentTrack(NewPCBLibComp, -7.95, -5.55, -7.95, 5.55, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -7.95, 5.55, 7.95, 5.55, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7.95, 5.55, 7.95, -5.55, eMechanical11, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 7.95, -5.55, -7.95, -5.55, eMechanical11, 0.12, False);
        CreateComponentArc(NewPCBLibComp, 0, 0, 0.25, 0, 360, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 0, 0.35, 0, -0.35, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -0.35, 0, 0.35, 0, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.205, -5.55, 8.08, -5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 8.08, -5.55, 8.08, 5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 8.08, 5.55, 6.205, 5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -6.205, -5.55, -8.08, -5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -8.08, -5.55, -8.08, 5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, -8.08, 5.55, -6.205, 5.55, eTopOverlay, 0.12, False);
        CreateComponentTrack(NewPCBLibComp, 8.15, -5.75, 8.15, 5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 8.15, 5.75, 6.225, 5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.225, 5.75, 6.225, 7.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.225, 7.91, -6.225, 7.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.225, 7.91, -6.225, 5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.225, 5.75, -8.15, 5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -8.15, 5.75, -8.15, -5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -8.15, -5.75, -6.225, -5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.225, -5.75, -6.225, -7.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, -6.225, -7.91, 6.225, -7.91, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.225, -7.91, 6.225, -5.75, eMechanical15, 0.05, False);
        CreateComponentTrack(NewPCBLibComp, 6.225, -5.75, 8.15, -5.75, eMechanical15, 0.05, False);

        STEPmodel := PcbServer.PCBObjectFactory(eComponentBodyObject, eNoDimension, eCreate_Default);
        Model := STEPmodel.ModelFactory_FromFilename('D:\_GitProject\ONLY_Altium_Libraries\step\SOIC\PowerSO20.STEP', false);
        STEPModel.Layer := eMechanical13;
        STEPmodel.Model := Model;
        STEPmodel.SetState_Identifier(NewPcbLibComp.Name);
        NewPCBLibComp.AddPCBObject(STEPmodel);

        CurrentLib.RegisterComponent(NewPCBLibComp);
        CurrentLib.CurrentComponent := NewPcbLibComp;
    Finally
        PCBServer.PostProcess;
    End;

    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All' , 255, Client.CurrentView)
End;

Procedure CreateAPCBLibrary(Zero : integer);
Var
    View     : IServerDocumentView;
    Document : IServerDocument;
    TempPCBLibComp : IPCB_LibComponent;

Begin
    If PCBServer = Nil Then
    Begin
        ShowMessage('No PCBServer present. This script inserts a footprint into an existing PCB Library that has the current focus.');
        Exit;
    End;

    CurrentLib := PcbServer.GetCurrentPCBLibrary;
    If CurrentLib = Nil Then
    Begin
        ShowMessage('You must have focus on a PCB Library in order for this script to run.');
        Exit;
    End;

    View := Client.GetCurrentView;
    Document := View.OwnerDocument;
    Document.Modified := True;

    // Create And focus a temporary component While we delete items (BugCrunch #10165)
    TempPCBLibComp := PCBServer.CreatePCBLibComp;
    TempPcbLibComp.Name := '___TemporaryComponent___';
    CurrentLib.RegisterComponent(TempPCBLibComp);
    CurrentLib.CurrentComponent := TempPcbLibComp;
    CurrentLib.Board.ViewManager_FullUpdate;

    CreateComponentPowerSO20(0);

    // Delete Temporary Footprint And re-focus
    CurrentLib.RemoveComponent(TempPCBLibComp);
    CurrentLib.Board.ViewManager_UpdateLayerTabs;
    CurrentLib.Board.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=All', 255, Client.CurrentView)
End;

Procedure CreateALibrary;
Begin
    Screen.Cursor := crHourGlass;

    CreateAPCBLibrary(0);

    Screen.Cursor := crArrow;
End;

End.
